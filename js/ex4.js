/**
 * 
 * Sử dụng vòng lặp for kết hợp với điều kiện if 
 * Nếu i chẵn thì in ra div màu đỏ 
 * Nếu i lẻ thì in ra div màu xanh s
 */




document.getElementById('taothediv').addEventListener('click', function(){
    for (var i = 1 ; i <=10 ; i++) {
        if (i %2 != 0) {
            var taoTheDivLe = document.createElement('DIV') ; 
            taoTheDivLe.innerHTML = `div lẻ ${i}` ; 
            taoTheDivLe.style.width = '100%' ; 
            taoTheDivLe.style.height = '30px' ; 
            taoTheDivLe.style.background = 'blue' ;
            taoTheDivLe.style.color = 'white' ;
            document.getElementById('show-ketqua4').appendChild(taoTheDivLe);
        }else {
            var taoTheDivChan = document.createElement('DIV') ; 
            taoTheDivChan.innerHTML = `div chẵn ${i}` ; 
            taoTheDivChan.style.width = '100%' ; 
            taoTheDivChan.style.height = '30px' ; 
            taoTheDivChan.style.background = 'red' ;
            taoTheDivChan.style.color = "white" ; 
            document.getElementById('show-ketqua4').appendChild(taoTheDivChan);

        }
    }
});